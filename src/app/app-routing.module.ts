import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/AuthGuard';
import { AuthGuardLogin } from './guard/AuthGuardLogin';

const routes: Routes = [
  { path: '', redirectTo: '/auths/login', pathMatch: 'full' },
  { path: 'auths', canActivate: [AuthGuardLogin], loadChildren: () => import('./page/login/login/login.module').then(m => m.LoginModule) },
  { path: 'register', canActivate: [AuthGuardLogin], loadChildren: () => import('./page/register/register/register.module').then(m => m.RegisterModule) },
  { path: 'admin' ,canActivate: [AuthGuard], loadChildren: () => import('./page/admin/admin/admin.module').then(m => m.AdminModule) },
  { path: 'notfound', loadChildren: () => import('./page/notfound/notfound/notfound.module').then(m => m.NotfoundModule) },
  { path: '**',  redirectTo: '/notfound', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
