import { Injectable } from '@angular/core';
import { RequestUser } from '../model/request/RequestUser';
import { RequestLogin } from '../model/request/RequestLogin';
import { ResponseUser } from '../model/response/ResponseUser';
import { ResponseLogin } from '../model/response/ResponseLogin';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private localStorageKey = 'users';
  private sessionStorageKey = 'currentUser';

  constructor() {}

  registerUser(user: RequestUser): Observable<boolean> {
    const existingUsers = this.getRegisteredUsers();
    const isEmailTaken = existingUsers.some(existingUser => existingUser.email === user.email);

    if (isEmailTaken) {      
      return of(false);
    }
    existingUsers.push(user);
    localStorage.setItem(this.localStorageKey, JSON.stringify(existingUsers));    
    return of(true);
  }

  loginUser(login: RequestLogin): boolean {
    const existingUsers = this.getRegisteredUsersPassword();
    const user = existingUsers.find(u => u.email === login.email && u.password === login.password);
    return !!user; 
  }  

  getRegisteredUsersPassword(): ResponseLogin[] {
    const usersString = localStorage.getItem(this.localStorageKey);
    return usersString ? JSON.parse(usersString) : [];
  }

  getRegisteredUsers(): ResponseUser[] {
    const usersString = localStorage.getItem(this.localStorageKey);
    return usersString ? JSON.parse(usersString) : [];
  }

  login(login: RequestLogin): Observable<boolean> {   
    // Lógica de autenticación aquí 
    const user = this.loginUser(login);
    if (user) {      
      sessionStorage.setItem(this.sessionStorageKey, JSON.stringify(user));
      return of(true);
    }
    return of(false);
  }

  logout(): void {
    // Eliminar el usuario de la sesión al cerrar sesión
    sessionStorage.removeItem(this.sessionStorageKey);
  }

  getCurrentUser(): ResponseUser | null {
    // Obtener el usuario actual almacenado en la sesión
    const userString = sessionStorage.getItem(this.sessionStorageKey);
    return userString ? JSON.parse(userString) : null;
  }

  isLoggedIn(): boolean {
    // Verificar si hay un usuario almacenado en la sesión
    return !!this.getCurrentUser();
  }
}
