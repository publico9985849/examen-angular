export interface ResponseUser {
  name: string;
  surname: string;
  email: string;  
}