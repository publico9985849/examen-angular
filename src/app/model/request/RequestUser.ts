export interface RequestUser {
  name: string;
  surname: string;
  email: string;
  password: string;    
}