import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/UserService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{
  hide = true;
  loginForm!: FormGroup;
  error: string = ""; 

  constructor(private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly userService:UserService){}

  ngOnInit(): void {
    this.loginForm = this.initForm();      
  }

  onSubmit(): void {    
    this.userService.login(this.loginForm.value).subscribe(
      success => {
        if (success) {          
          this.router.navigate(['/admin']);
          this.loginForm.reset();
          console.log('inicio correcto');
          this.error = ""          
        } else {          
          this.error = "Error al inicar sesion"          
          console.log('Error al inicar sesion');
        }
      }
    )
  }

  initForm(): FormGroup {
    return this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }
}