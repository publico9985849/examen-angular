import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ResponseUser } from 'src/app/model/response/ResponseUser';
import { UserService } from 'src/app/service/UserService';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements AfterViewInit {
  constructor(private readonly router: Router,
    private readonly userService:UserService){}

  displayedColumns: string[] = ['name', 'surname', 'email'];  
  dataUser = new MatTableDataSource<ResponseUser>(this.userService.getRegisteredUsers())

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit() {    
    this.dataUser.paginator = this.paginator;    
  }


  logout(){
    this.userService.logout()
    this.router.navigate(['/auths/login']);
  }
}