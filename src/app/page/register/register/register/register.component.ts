import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/UserService';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit{
  hide = true;
  registerForm!: FormGroup;  
  error:string = "";

  constructor(private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly userService:UserService){}

  ngOnInit(): void {
    this.registerForm = this.initForm();      
  }

  onSubmit(): void {
    this.userService.registerUser(this.registerForm.value).subscribe(
      success => {
        if (success) {          
          this.router.navigate(['/auths/login']);
          this.registerForm.reset();
          console.log('Registration successful');
          this.error = ""          
        } else {          
          this.error = "Error al registrar"          
          console.log('Error al registrar');
        }
      }
    );
  }

  initForm(): FormGroup {
    return this.fb.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }
}
